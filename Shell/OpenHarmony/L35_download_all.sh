#!/bin/sh
# download OpenHarmony
mkdir OpenHarmony_Large_System # 创建OpenHarmony工作目录
cd OpenHarmony_Large_System # 切换到OpenHarmony工作目录
repo init -u https://gitee.com/open-harmony2/manifest.git -b master_L35 --no-repo-verify # repo初始化
repo forall -c 'git reset --hard;git clean -fd'
repo sync -c # 更新代码
repo forall -c 'git lfs pull' # 更新二进制
cd - # 切换到之前的目录

mkdir aosp_android-10.0.0_r2_for_OpenHarmony # 创建AOSP工作目录
cd aosp_android-10.0.0_r2_for_OpenHarmony # 切换到AOSP工作目录
repo init -u git://mirrors.ustc.edu.cn/aosp/platform/manifest -b android-10.0.0_r2 --depth=1 # repo初始化
repo sync -c # 更新代码
cd - # 切换到之前的目录
rm -rf OpenHarmony_Large_System/third_party/aosp
cp -Rp android-10.0.0_r2_for_OpenHarmony  OpenHarmony_Large_System/third_party/aosp # 将AOSP的代码移动到OpenHarmony_Large_System/third_party/aosp目录下
cd OpenHarmony_Large_System/third_party/aosp/patches/10.0.0.r2;bash ./make_patch.sh # 安装AOSP补丁
cd -
cd OpenHarmony_Large_System/kernel/ # 切换到kernel工作目录
git clone -b v4.19.90 https://gitee.com/linuxlabs/linux-stable.git linux-4.19 --depth=1 # 下载指定版本代码
cd OpenHarmony_Large_System # 切换到OpenHarmony工作目录
curl https://gitee.com/landwind/script-tools/raw/master/Shell/OpenHarmony/LargeSystem_prebuilts_download.sh >./LargeSystem_prebuilts_download.sh # 下载脚本
# 二进制默认存放/tmp/prebuilts,如需修改默认位置,请编辑LargeSystem_prebuilts_download.sh修改bin_dir值
bash ./LargeSystem_prebuilts_download.sh # 下载并解压prebuilts压缩包到指定位置
mkdir -p OpenHarmony_Large_System/prebuilts/build-tools/common/nodejs;cd OpenHarmony_Large_System/prebuilts/build-tools/common/nodejs # 创建NodeJS目录并切换
wget --no-check-certificate https://nodejs.org/download/release/v12.18.4/node-v12.18.4-linux-x64.tar.gz # 下载nodejs安装包
tar -zxvf node-v12.18.4-linux-x64.tar.gz # 解压nodejs压缩包
cd -
export PATH=${PATH}:OpenHarmony_Large_System/prebuilts/build-tools/common/nodejs/node-v12.18.4-linux-x64/bin # 设置NodeJS环境变量
cd OpenHarmony_Large_System/third_party/jsframework # 进入jsframework目录
npm install # 下载node_modules包
cp -Rp OpenHarmony_Large_System/third_party/jsframework/node_modules OpenHarmony_Large_System/prebuilts/build-tools/common/js-framework # 把下载的node_modules包放入OpenHarmony代码的prebuilts/build-tools/common/js-framework目录下