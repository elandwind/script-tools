#!/bin/bash
# 代码下载目录
code_dir=.
# 二进制所在目录,用于临时存放二进制,二进制整个大小约10G,请确保有足够的空间
bin_dir=./hwcloud_prebuilts
# 二进制关系
copy_config="""aa-1.0.tar.gz,prebuilts/build-tools/common/aafwk,https://repo.huaweicloud.com/harmonyos/compiler/aafwk/1.0/aa-1.0.tar.gz
cmake-darwin-x86-3.16.5.tar.gz,prebuilts/cmake,https://repo.huaweicloud.com/harmonyos/compiler/cmake/3.16.5/darwin/cmake-darwin-x86-3.16.5.tar.gz
cmake-linux-x86-3.16.5.tar.gz,prebuilts/cmake,https://repo.huaweicloud.com/harmonyos/compiler/cmake/3.16.5/linux/cmake-linux-x86-3.16.5.tar.gz
cmake-windows-x86-3.16.5.tar.gz,prebuilts/cmake,https://repo.huaweicloud.com/harmonyos/compiler/cmake/3.16.5/windows/cmake-windows-x86-3.16.5.tar.gz
d8-1.5.13.tar.gz,prebuilts,https://repo.huaweicloud.com/harmonyos/compiler/d8/1.5.13/d8-1.5.13.tar.gz
gn-darwin-x86-1717.tar.gz,prebuilts/build-tools/darwin-x86/bin,https://repo.huaweicloud.com/harmonyos/compiler/gn/1717/darwin/gn-darwin-x86-1717.tar.gz
gn-linux-x86-1717.tar.gz,prebuilts/build-tools/linux-x86/bin,https://repo.huaweicloud.com/harmonyos/compiler/gn/1717/linux/gn-linux-x86-1717.tar.gz
idl-1.0.tar.gz,prebuilts/build-tools/common/aafwk,https://repo.huaweicloud.com/harmonyos/compiler/idl/1.0/idl-1.0.tar.gz
llvm-darwin-x86_64-9.0.0-36191-2.0.tar.gz,prebuilts/clang/ohos/darwin-x86_64,https://repo.huaweicloud.com/harmonyos/compiler/clang/9.0.0-36191/darwin/llvm-darwin-x86_64-9.0.0-36191-2.0.tar.gz
llvm-linux-x86_64-9.0.0-36191-2.0.tar.gz,prebuilts/clang/ohos/linux-x86_64,https://repo.huaweicloud.com/harmonyos/compiler/clang/9.0.0-36191/linux/llvm-linux-x86_64-9.0.0-36191-2.0.tar.gz
llvm-windows-x86_64-9.0.0-36191-2.0.tar.gz,prebuilts/clang/ohos/windows-x86_64,https://repo.huaweicloud.com/harmonyos/compiler/clang/9.0.0-36191/windows/llvm-windows-x86_64-9.0.0-36191-2.0.tar.gz
ndk-1.0.tar.gz,prebuilts,https://repo.huaweicloud.com/harmonyos/compiler/ndk/1.0/ndk-1.0.tar.gz
ninja-darwin-x86-1.10.1.tar.gz,prebuilts/build-tools/darwin-x86/bin,https://repo.huaweicloud.com/harmonyos/compiler/ninja/1.10.1/darwin/ninja-darwin-x86-1.10.1.tar.gz
ninja-linux-x86-1.10.1.tar.gz,prebuilts/build-tools/linux-x86/bin,https://repo.huaweicloud.com/harmonyos/compiler/ninja/1.10.1/linux/ninja-linux-x86-1.10.1.tar.gz
python-darwin-x86-3.8.5.tar.gz,prebuilts/python,https://repo.huaweicloud.com/harmonyos/compiler/python/3.8.5/darwin/python-darwin-x86-3.8.5.tar.gz
python-linux-x86-3.8.5.tar.gz,prebuilts/python,https://repo.huaweicloud.com/harmonyos/compiler/python/3.8.5/linux/python-linux-x86-3.8.5.tar.gz
restool-1.016.tar.gz,prebuilts/build-tools/common,https://repo.huaweicloud.com/harmonyos/compiler/restool/1.016/restool-1.016.tar.gz
signcenter-V2.0-2020.09.10.tar.gz,prebuilts,https://repo.huaweicloud.com/harmonyos/compiler/signcenter/V2.0-2020.09.10/signcenter-V2.0-2020.09.10.tar.gz
"""


for i in `echo ${copy_config}`
do
    bin_file=`echo $i|awk -F ',' '{print $1}'`
    unzip_dir=`echo $i|awk -F ',' '{print $2}'`
    huaweicloud_url=`echo $i|awk -F ',' '{print $3}'`
    huaweicloud_file_name=`echo ${huaweicloud_url}|awk -F '/' '{print $NF}'`
    if [ "${huaweicloud_file_name}" != "${bin_file}" ];then
        echo "copy_config配置不正确,提供二进制名与下载二进制文件不匹配,请检查"
        echo "copy_config error,The provided binary name does not match the downloaded binary file. Please check."
        exit
    fi
    if [ ! -d "${code_dir}/${unzip_dir}" ];then
        mkdir -p "${code_dir}/${unzip_dir}"
    fi
    if [ ! -f "${bin_dir}/${bin_file}" ];then
        # 代理不需要鉴权: wget -P ${bin_dir} -e "https_proxy=http://domain.com:port" ${huaweicloud_url}
        # 代理需要鉴权(账号密码特殊字符均需要URL转义): wget -P ${bin_dir} -e "https_proxy=http://username:password@domain.com:port" ${huaweicloud_url}
        # 不需要代理
        wget -P ${bin_dir}  ${huaweicloud_url}
    fi
    tar -xvzf "${bin_dir}/${bin_file}"  -C  "${code_dir}/${unzip_dir}"
done
