#!/bin/sh
# 请在repo init目录执行,其余目录会报 找不到路径
# 获取repo sync 下载代码仓库的大小,可以统计裸仓与工作目录大小
# 裸仓大小: 可能因为存在历史commit或者多条分支导致统计值变大,仅供参考
# 工作目录大小:请注意,可能因为path重合导致重复统计,仅供参考
RUN_FOLDER=$(cd "$(dirname "${PWD##*/}")";pwd)

repo list>${RUN_FOLDER}/repo.list
echo "Name,Path,Bare_Repo_Size(M),WorkTree_Size(M)">${RUN_FOLDER}/repo_size.csv
while read ONE_REPO
do
    repo_path=`echo ${ONE_REPO}|awk '{print $1}'`
    repo_name=`echo ${ONE_REPO}|awk '{print $NF}'`
    bare_size=`du -sm ${RUN_FOLDER}/.repo/project-objects/${repo_name}.git|awk  '{print $1}'`
    worktree_size=`du -sm ${RUN_FOLDER}/${repo_path}|awk  '{print $1}'`
    echo "${repo_name},${repo_path},${bare_size},${worktree_size}">>${RUN_FOLDER}/repo_size.csv
done<${RUN_FOLDER}/repo.list
echo "Calc Size Over,Result is ${RUN_FOLDER}/repo_size.csv"